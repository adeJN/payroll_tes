<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Hitung_gaji extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('logged_in')) {
            redirect('auth');
        }
        $this->load->model('M_karyawan');
    }

    public function index()
    {
        $data['allKaryawan'] = $this->M_karyawan->getAll();

        $this->load->view('temp/header');
        $this->load->view('adm/hitung_gaji_by_bulan', $data);
        $this->load->view('temp/footer');
    }
    public function by_bulan()
    {
        $startDate = $this->input->post('tggl_awal');
        $endDate = $this->input->post('tggl_akhir');

        $maxDays = date('t') - 8;

        // $this->db->select('*, (' . $maxDays . '-count(tanggal) ) as total_masuk, ');

        $this->db->select('*,SUM(IF
		(
			lembur <= 4,
			( lembur * 1 ),
		( lembur * 2 )) 
		) as  total_lembur,( ' . $maxDays . '-count(tanggal) ) as total_masuk, ');
        $this->db->from('karyawan');
        $this->db->join('presensi', 'karyawan.karyawan_id = presensi.fk_karyawan_id');
        $this->db->where('tanggal >=', $startDate);
        $this->db->where('tanggal <=', $endDate);
        $this->db->group_by('karyawan_id');

        $data['allKaryawan']   = $this->db->get()->result();
        $data['tanggal_awal']  = $startDate;
        $data['tanggal_akhir'] = $endDate;

        // print_r($data['allKaryawan']);

        // $data['allKaryawan'] = $this->M_karyawan->getAll();

        $this->load->view('temp/header');
        $this->load->view('adm/hitung_gaji', $data);
        $this->load->view('temp/footer');
    }
    function sahkan($id = null)
    {
        $data = array(
            'status_cek'                  => 1
        );
        $this->db->update('karyawan', $data, array('karyawan_id' => $id));
        echo "<script>alert('Gaji Karyawan Telah disahkan'); window.location.href='../';</script>";
    }

    function cetak()
    {
        $data['nama'] = $this->input->post('nama');
        $data['gaji_pokok'] = $this->input->post('gaji_pokok');
        $data['tunjangan'] = $this->input->post('tunjangan');
        $data['lembur'] = $this->input->post('lembur');
        $data['bpjs'] = $this->input->post('bpjs');
        $data['nwnp'] = $this->input->post('nwnp');
        $data['total_gaji'] = $this->input->post('total_gaji');
        $data['tanggal_awal'] = $this->input->post('tanggal_awal');
        $data['tanggal_akhir'] = $this->input->post('tanggal_akhir');
        $this->load->view('adm/gaji_pdf', $data);
    }
    function export_pdf($satu, $dua, $tiga, $empat, $lima, $enam, $tujuh, $delapan, $sembilan)
    {
        $data['nama'] = urldecode($satu);
        $data['gaji_pokok'] = $dua;
        $data['tunjangan'] = $tiga;
        $data['lembur'] = $empat;
        $data['bpjs'] = $lima;
        $data['nwnp'] = $enam;
        $data['total_gaji'] = $tujuh;
        $data['tanggal_awal'] = $delapan;
        $data['tanggal_akhir'] = $sembilan;
        // $this->load->view('adm/gaji_pdf', $data);
        $this->load->library('pdf');

        //$this->pdf->setPaper('A4', 'potrait');

        $customPaper = array(0, 0, 300, 330);
        $this->pdf->setPaper($customPaper);
        date_default_timezone_set("Asia/Bangkok");
        // echo "The time is " . date("h:i:sa");
        $this->pdf->filename = "gaji-" . $data['nama'] . "-" . date("His") . ".pdf";

        $this->pdf->load_view('adm/gaji_pdf', $data);
    }
}
