# Payroll_tes

-----------------------
**FRAMEWORK** : **CODEIGNITER** 3
-
DATABASE : Mysql
-
database_name : payroll_tes
------------------------

**user **
---------------------
Payroll Staff
-------------
    username : staff
    password : 12345
Payroll Supervisor
------------
    username : supervisor
    password : 12345
---------------------
**ALUR SISTEM**
-------

1. login dengan user payroll staff
2. Maka tampil dashboard staff
3. Klik pada navbar karyawan atau bagian karyawan jika ingin memasukkan, mengubah, dan menghapus informasi karyawan
4. Klik pada navbar presensi jika ingin membuat absensi dari karyawan (terdapat data kehadiran dari karyawan)
5. Klik pada navbar gaji karyawan jika ingin menghitung gaji dan melihat hasil perhitungan gaji 
caraa :
- pilih range tanggal karyawan yang akan dihitung gajinya terlebih dahulu (**jika ingin tau bulan karyawan hadir lihat di presensi)
- setelah range tanggal terpilih maka akan tampil hasil dari perhitungan gaji semua karyawan yang telah dimasukkan data presensinya 
6. setelah itu logout 
7. login dengan user payroll supervisor
8. klik pada gaji karyawan setelah itu pilih range tanggal dan akan tampil perhitungan gaji dan klik button sahkan pada tabel
9. maka staff bisa eksport data gaji
