<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_karyawan extends CI_Model
{
    private $table = "karyawan";

    function getAll()
    {
        return $this->db->get($this->table)->result();
    }

    function tambah_karyawan()
    {
        $data = array(
            'nama'                  => $this->input->post('nama'),
            'tempat_lahir'          => $this->input->post('tempat_lahir'),
            'tanggal_lahir'         => $this->input->post('tanggal_lahir'),
            'jenis_kelamin'         => $this->input->post('jk'),
            'gaji_pokok'            => $this->input->post('gaji_pokok'),
            'tunjangan'             => $this->input->post('tunjangan')
        );

        return $this->db->insert($this->table, $data);
    }

    function edit_karyawan($data, $id)
    {
        if (!empty($data) && !empty($id)) {
            $update = $this->db->update($this->table, $data, array('karyawan_id' => $id));
            return $update ? true : false;
        } else {
            return false;
        }
    }

    function hapus_karyawan($id)
    {
        if (!empty($id)) {
            $delete = $this->db->delete($this->table, array('karyawan_id' => $id));
            return $delete ? true : false;
        } else {
            return false;
        }
    }
}
