<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Data</li>
        </ol>
        <!-- Example DataTables Card-->
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-list"></i> Pilih Range tanggal
                <!-- <button class="btn btn-success" style="float:right;"><i class="fa fa-plus"></i> Tambah</button> -->
            </div>
            <div class="card-body">
                <?php echo form_open('hitung_gaji/by_bulan'); ?>

                <label for="">Tanggal Awal</label>
                <input type="date" name="tggl_awal" class="form-control">
                <label for="">Tanggal Akhir</label>
                <input type="date" name="tggl_akhir" class="form-control">
                <br>
                <input type="submit" name="" id="" class="btn btn-primary">
                </form>
            </div>
            <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
    </div>
    <!-- /.container-fluid-->