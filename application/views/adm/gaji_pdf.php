<html>

<head>
    <title>struk</title>
    <style>
        @page {
            size: auto;
            size: 4.2in 3.6in;
            margin: 0mm;
        }

        @media print {
            .noPrint {
                display: none;
            }
        }
    </style>
</head>

<body>
    <table style="margin-left: auto;margin-right: auto;">
        <tr>
            <td colspan="3">
                <center><b>PT. Mekar Jaya
            </td>
        </tr>
        <tr>
            <td>Nama</td>
            <td>:</td>
            <td><?= $nama; ?></td>
        </tr>
        <tr>
            <td>Periode</td>
            <td>:</td>
            <td>
                <?php echo date('d F Y', strtotime($tanggal_awal)); ?> S/D <?php echo date('d F Y', strtotime($tanggal_akhir)); ?> </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td colspan="3">
                A. Upah Tetap
            <td>
        </tr>
        <tr>
            <td>- Gaji Pokok</td>
            <td>:</td>
            <td><?php echo "Rp " . number_format($gaji_pokok, 2, ',', '.'); ?></td>
        </tr>
        <tr>
            <td>- Tunjangan</td>
            <td>:</td>
            <td><?php echo "Rp " . number_format($tunjangan, 2, ',', '.'); ?></td>
        </tr>
        <tr>
            <td colspan="3">
                B. Upah Tidak Tetap
            <td>
        </tr>
        <tr>
            <td>- Lembur</td>
            <td>:</td>
            <td><?php echo "Rp " . number_format($lembur, 2, ',', '.'); ?></td>
        </tr>
        <tr>
            <td colspan="3">
                C. Potongan
            <td>
        </tr>
        <tr>
            <td>- BPJS</td>
            <td>:</td>
            <td><?php echo "Rp " . number_format($bpjs, 2, ',', '.'); ?></td>
        </tr>
        <tr>
            <td>- NWNP</td>
            <td>:</td>
            <td><?php echo "Rp " . number_format($nwnp, 2, ',', '.'); ?></td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td>Total Penerimaan</td>
            <td>:</td>
            <td><?php echo "Rp " . number_format($total_gaji, 2, ',', '.'); ?></td>
        </tr>
    </table>
    <center><a href="<?= base_url(); ?>hitung_gaji" class="noPrint">
            <<< </a>
    </center>
    <script>
        window.print()
    </script>

</body>

</html>