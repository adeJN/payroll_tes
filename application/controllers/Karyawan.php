<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Karyawan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('logged_in')) {
            redirect('auth');
        }
        $this->load->model('M_karyawan');
    }
    public function index()
    {
        $data['title'] = "Staff Payroll";

        $data['allKaryawan'] = $this->M_karyawan->getAll();

        $this->load->view('temp/header');
        $this->load->view('adm/karyawan', $data);
        $this->load->view('adm/karyawan_tambah');
        $this->load->view('adm/karyawan_edit', $data);
        $this->load->view('temp/footer');
    }
    function tambah()
    {
        $this->M_karyawan->tambah_karyawan();
        echo "<script>alert('Data Karyawan Berhasil dimasukkan'); window.location.href='../karyawan';</script>";
    }
    function edit($id = null)
    {
        $post_data = array(
            'nama'                  => $this->input->post('nama'),
            'tempat_lahir'          => $this->input->post('tempat_lahir'),
            'tanggal_lahir'         => $this->input->post('tanggal_lahir'),
            'jenis_kelamin'         => $this->input->post('jk'),
            'gaji_pokok'            => $this->input->post('gaji_pokok'),
            'tunjangan'             => $this->input->post('tunjangan')
        );
        $this->M_karyawan->edit_karyawan($post_data, $id);
        echo "<script>alert('Data Karyawan Berhasil Diubah'); window.location.href='../';</script>";
    }
    function hapus($id)
    {
        $this->M_karyawan->hapus_karyawan($id);
        echo "<script>alert('Data telah terhapus!'); window.location.href='../';</script>";
    }
}
