<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Presensi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('logged_in')) {
            redirect('auth');
        }
        $this->load->model('M_karyawan');
    }

    public function index()
    {
        $maxDays = date('t') - 8;

        $this->db->select('*,SUM(IF
		(lembur <= 4,( lembur * 1 ),( lembur * 2 )) 
		) as  total_lembur,( ' . $maxDays . '-count(tanggal)) as nwnp, count(tanggal) as total_hadir');
        $this->db->from('karyawan');
        $this->db->join('presensi', 'karyawan.karyawan_id = presensi.fk_karyawan_id');
        $this->db->group_by('karyawan_id');

        $data['allKaryawan'] = $this->db->get()->result();

        $this->db->select('*');
        $this->db->from('presensi');
        $data['allPresensi'] = $this->db->get()->result();

        $this->db->select('*');
        $this->db->from('karyawan');
        $data['allllllKaryawan'] = $this->db->get()->result();

        $this->load->view('temp/header');
        $this->load->view('adm/presensi', $data);
        $this->load->view('adm/presensi_tambah_absen_karyawan', $data);
        $this->load->view('adm/presensi_tambah_absen_karyawan2', $data);
        $this->load->view('temp/footer');
    }
    function tambah_absen()
    {
        $data = array(
            'fk_karyawan_id'   => $this->input->post('id_karyawan'),
            'tanggal'          => $this->input->post('tanggal'),
            'lembur'           => $this->input->post('lembur')
        );

        $this->db->insert("presensi", $data);
        echo "<script>alert('Data absen karyawan Berhasil ditambah'); window.location.href='../presensi';</script>";
    }
}
