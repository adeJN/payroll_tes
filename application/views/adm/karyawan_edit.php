<!-- Modal Edit -->
<?php foreach ($allKaryawan as $row) { ?>
    <div id="modalEdit<?= $row->karyawan_id ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="<?= site_url('karyawan/edit/' . $row->karyawan_id) ?>" method="post">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Edit Data Karyawan</h3>
                        <br>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body mb-3">

                        <!-- Content -->
                        <input type="text" class="form-control" placeholder="Nama" name="nama" value="<?= $row->nama; ?>" style="margin-bottom: 12px;" required>
                        <div class="form-group">
                            <input type="text" name="tempat_lahir" class="form-control" placeholder="Masukkan Tempat Lahir" value="<?= $row->tempat_lahir; ?>" required>
                        </div>
                        <div class="form-group">
                            <label for="">Tanggal Lahir</label>
                            <input type="date" value="<?= $row->tanggal_lahir; ?>" name="tanggal_lahir" class="form-control" placeholder="Masukkan Tanggal Lahir" required>
                        </div>
                        <div class="form-group">
                            <!-- <input type="text" class="form-control" placeholder="Masukkan Jenis Kelamin">   -->
                            <select name="jk" id="" class="form-control">
                                <option value="">Pilih Jenis Kelamin</option>
                                <option value="L" <?php if ($row->jenis_kelamin == "L") {
                                                        echo "selected";
                                                    } ?>>Laki laki</option>
                                <option value="P" <?php if ($row->jenis_kelamin == "P") {
                                                        echo "selected";
                                                    } ?>>Perempuan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="number" value="<?= $row->gaji_pokok; ?>" name="gaji_pokok" class="form-control" placeholder="Masukkan Gaji Pokok" required>
                        </div>
                        <div class="form-group">
                            <input type="number" value="<?= $row->tunjangan; ?>" name="tunjangan" class="form-control" placeholder="Masukkan Tunjangan" required>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary" type="submit">Submit</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php } ?>