<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Data</li>
        </ol>
        <!-- Example DataTables Card-->
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-list"></i> Data Gaji Karyawan
                <!-- <button class="btn btn-success" style="float:right;"><i class="fa fa-plus"></i> Tambah</button> -->
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Gaji Pokok</th>
                                <th>Tunjangan</th>
                                <th>(-)BPJS</th>
                                <th>(+)Lembur</th>
                                <th>(-)NWNP</th>
                                <th>TOTAL GAJI</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Gaji Pokok</th>
                                <th>Tunjangan</th>
                                <th>(-)BPJS</th>
                                <th>(+)Lembur</th>
                                <th>(-)NWNP</th>
                                <th>TOTAL GAJI</th>
                                <th>Aksi</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($allKaryawan as $row) {
                            ?>
                                <tr>
                                    <td widtd="5%"><?php echo $no++; ?></td>
                                    <td><?php echo $row->nama; ?></td>
                                    <td><?php echo "Rp " . number_format($row->gaji_pokok, 2, ',', '.'); ?></td>
                                    <td><?php echo "Rp " . number_format($row->tunjangan, 2, ',', '.'); ?></td>
                                    <td>
                                        <?php
                                        $bpjs = ($row->gaji_pokok + $row->tunjangan) * 0.03;
                                        echo "Rp " . number_format($bpjs, 2, ',', '.');
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $upah_lembur_per_jam = $row->gaji_pokok / 173;
                                        $jam_lembur = $row->total_lembur;
                                        $total_upah_lembur = $jam_lembur * $upah_lembur_per_jam;
                                        echo "Rp " . number_format($total_upah_lembur, 2, ',', '.')
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $nwnp = ($row->total_masuk * $row->gaji_pokok) / 30;
                                        echo "Rp " . number_format($nwnp, 2, ',', '.');
                                        ?>
                                    </td>
                                    <th>
                                        <?php
                                        $total_gaji = $row->gaji_pokok + $row->tunjangan + $total_upah_lembur - $nwnp - $bpjs;
                                        echo "Rp " . number_format($total_gaji, 2, ',', '.');
                                        ?>
                                    </th>
                                    <td>
                                        <?php if ($this->session->userdata('level') == 'staff' || $this->session->userdata('level') == 'superadmin') {
                                            if ($row->status_cek == 1) {
                                        ?>
                                                <?php echo form_open('hitung_gaji/cetak'); ?>

                                                <input type="hidden" name="nama" value="<?= $row->nama; ?>">
                                                <input type="hidden" name="gaji_pokok" value="<?= $row->gaji_pokok; ?>">
                                                <input type="hidden" name="tunjangan" value="<?= $row->tunjangan; ?>">
                                                <input type="hidden" name="lembur" value="<?= $total_upah_lembur; ?>">
                                                <input type="hidden" name="bpjs" value="<?= $bpjs; ?>">
                                                <input type="hidden" name="nwnp" value="<?= $nwnp; ?>">
                                                <input type="hidden" name="total_gaji" value="<?= $total_gaji; ?>">
                                                <input type="hidden" name="tanggal_awal" value="<?= $tanggal_awal; ?>">
                                                <input type="hidden" name="tanggal_akhir" value="<?= $tanggal_akhir; ?>">

                                                <button type="submit" class="btn btn-warning"><i class="fa fa-print"></i> cetak</button>
                                                </form>

                                            <?php } ?>

                                            <a href="<?= base_url() ?>hitung_gaji/export_pdf/<?= $row->nama . '/' . $row->gaji_pokok . '/' . $row->tunjangan . '/' . $total_upah_lembur . '/' . $bpjs . '/' . $nwnp . '/' . $total_gaji . '/' . $tanggal_awal . '/' . $tanggal_akhir; ?>" class="btn btn-danger"><i class="fa fa-file"></i> pdf</a>
                                            </form>
                                        <?php
                                        } ?>
                                        <?php if ($this->session->userdata('level') == 'supervisor'  || $this->session->userdata('level') == 'superadmin') {
                                            if ($row->status_cek == 0) {
                                        ?>
                                                <a href="<?php echo base_url(); ?>hitung_gaji/sahkan/<?= $row->karyawan_id; ?>" class="btn btn-danger"><i class=" fa fa-times"></i> Sahkan</a>
                                            <?php } else {
                                            ?>
                                                <a href="" class="btn btn-success"><i class=" fa fa-check"></i> Telah Disahkan</a>
                                        <?php
                                            }
                                        } ?>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
    </div>
    <!-- /.container-fluid-->