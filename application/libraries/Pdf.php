<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * CodeIgniter DomPDF Library
 *
 * Generate PDF's from HTML in CodeIgniter
 *
 * @packge        CodeIgniter
 * @subpackage        Libraries
 * @category        Libraries
 * @author        Ardianta Pargo
 * @license        MIT License
 * @link        https://github.com/ardianta/codeigniter-dompdf
 */
use Dompdf\Dompdf;
use Dompdf\Options;
class Pdf extends Dompdf{
    /**
     * PDF filename
     * @var String
     */
    public $filename;
    public function __construct(){
        parent::__construct();
        $this->filename = "laporan.pdf";
    }
    /**
     * Get an instance of CodeIgniter
     *
     * @access    protected
     * @return    void
     */
    protected function ci()
    {
        return get_instance();
    }
    /**
     * Load a CodeIgniter view into domPDF
     *
     * @access    public
     * @param    string    $view The view to load
     * @param    array    $data The view data
     * @return    void
     */
    public function load_view($view, $data = array()){
        $options = new Options();
        $options->setChroot(FCPATH); //Set root nya ke /var/www/html/nama-project
        $options->setDefaultFont('courier');

        $this->setOptions($options);

        $html = $this->ci()->load->view($view, $data, TRUE);
        $this->load_html($html);
        // Render the PDF
        $this->render();

        // $output = $this->output();
        // file_put_contents($_SERVER['DOCUMENT_ROOT'].'/pdf/'.$this->filename, $output);

         // Output the generated PDF to Browser
        $this->stream($this->filename, array("Attachment" => false));
    }


    public function download($html, $filename='', $stream=TRUE, $paper = 'A4', $orientation = "portrait")
    {

       

        $dompdf = new DOMPDF();

         $options = new Options();
        $options->setChroot(FCPATH); //Set root nya ke /var/www/html/nama-project
        $options->setDefaultFont('courier');

        $dompdf->setOptions($options);


        $dompdf->loadHtml($html);
        $dompdf->setPaper($paper, $orientation);
        $dompdf->render();
        // if ($stream) {
        //     $dompdf->stream($filename.".pdf", array("Attachment" => 0));
        // } else {
        $file = $filename.".pdf";
            $output = $dompdf->output();
            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/app/pdf/'.$file, $output);
        //}
    }


}