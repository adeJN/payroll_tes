<!-- Modal Edit -->
<?php foreach ($allKaryawan as $row) { ?>
    <div id="modalabsen<?= $row->karyawan_id ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="<?= site_url('presensi/tambah_absen') ?>" method="post">
                    <div class="modal-header">
                        <h3 class="modal-title" id="exampleModalLabel">Data Absensi <?= $row->nama ?></h3>
                        <br>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">

                        <div class="form-group">
                            <label for="">Masukkan tanggal hadir</label>
                            <input type="hidden" name="id_karyawan" value="<?= $row->karyawan_id; ?>" class="form-control">
                            <input type="date" name="tanggal" class="form-control" required>
                        </div>
                        <div class="form-group">
                            <label for="">Masukkan Jumlah lembur</label>
                            <input type="number" min="0" name="lembur" class="form-control" required>
                        </div>
                        <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary" type="submit">Submit</button>

                </form>
            </div>
            <div class="modal-footer">
                <table class="table table-bordered" id="dataTable2" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Lembur</th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th>No</th>
                            <th>Tanggal</th>
                            <th>Lembur</th>
                        </tr>
                    </tfoot>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($allPresensi as $roww) {
                            if ($roww->fk_karyawan_id == $row->karyawan_id) {
                        ?>
                                <tr>
                                    <td widtd="5%"><?php echo $no++; ?></td>
                                    <td><?php echo $roww->tanggal; ?></td>
                                    <td><?php echo $roww->lembur; ?> Jam</td>
                                </tr>
                        <?php
                            }
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    </div>
<?php } ?>