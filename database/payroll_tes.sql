/*
 Navicat Premium Data Transfer

 Source Server         : lokal
 Source Server Type    : MySQL
 Source Server Version : 100414
 Source Host           : localhost:3306
 Source Schema         : payroll_tes_behaestex

 Target Server Type    : MySQL
 Target Server Version : 100414
 File Encoding         : 65001

 Date: 15/08/2021 22:39:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for karyawan
-- ----------------------------
DROP TABLE IF EXISTS `karyawan`;
CREATE TABLE `karyawan`  (
  `karyawan_id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tempat_lahir` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal_lahir` date NULL DEFAULT NULL,
  `jenis_kelamin` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `gaji_pokok` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tunjangan` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status_cek` int NOT NULL,
  `data_dibuat` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`karyawan_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of karyawan
-- ----------------------------
INSERT INTO `karyawan` VALUES (1, 'qori', 'Banyuwangi', '1998-08-12', 'P', '2000000', '100000', 1, '2021-08-15 22:17:55');
INSERT INTO `karyawan` VALUES (2, 'Ade Fajar', 'Tuban', '1998-01-09', 'L', '4000000', '600000', 0, '2021-08-15 13:31:48');
INSERT INTO `karyawan` VALUES (3, 'Rendra ', 'Malang', '1998-09-19', 'L', '3000000', '600000', 0, '2021-08-15 13:32:25');

-- ----------------------------
-- Table structure for presensi
-- ----------------------------
DROP TABLE IF EXISTS `presensi`;
CREATE TABLE `presensi`  (
  `presensi_id` int NOT NULL AUTO_INCREMENT,
  `fk_karyawan_id` int NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `lembur` int NOT NULL,
  PRIMARY KEY (`presensi_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of presensi
-- ----------------------------
INSERT INTO `presensi` VALUES (1, 1, '2021-08-02', 1);
INSERT INTO `presensi` VALUES (2, 1, '2021-08-03', 2);
INSERT INTO `presensi` VALUES (3, 1, '2021-08-04', 2);
INSERT INTO `presensi` VALUES (4, 1, '2021-08-05', 1);
INSERT INTO `presensi` VALUES (5, 1, '2021-08-06', 0);
INSERT INTO `presensi` VALUES (6, 1, '2021-08-09', 0);
INSERT INTO `presensi` VALUES (7, 1, '2021-08-10', 0);
INSERT INTO `presensi` VALUES (8, 1, '2021-08-11', 0);
INSERT INTO `presensi` VALUES (9, 1, '2021-08-12', 0);
INSERT INTO `presensi` VALUES (10, 1, '2021-08-13', 0);
INSERT INTO `presensi` VALUES (11, 1, '2021-08-16', 0);
INSERT INTO `presensi` VALUES (12, 1, '2021-08-17', 6);
INSERT INTO `presensi` VALUES (13, 1, '2021-08-18', 2);
INSERT INTO `presensi` VALUES (14, 1, '2021-08-19', 2);
INSERT INTO `presensi` VALUES (15, 2, '2021-08-19', 3);
INSERT INTO `presensi` VALUES (16, 2, '2021-08-20', 5);
INSERT INTO `presensi` VALUES (17, 2, '2021-08-21', 2);
INSERT INTO `presensi` VALUES (18, 2, '2021-08-22', 3);
INSERT INTO `presensi` VALUES (19, 2, '2021-08-23', 9);
INSERT INTO `presensi` VALUES (20, 3, '2021-08-15', 4);
INSERT INTO `presensi` VALUES (21, 3, '2021-08-15', 4);
INSERT INTO `presensi` VALUES (22, 3, '2021-08-16', 2);
INSERT INTO `presensi` VALUES (23, 3, '2021-08-16', 2);
INSERT INTO `presensi` VALUES (24, 3, '2021-08-18', 1);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `level` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `data_dibuat` timestamp(0) NOT NULL DEFAULT current_timestamp(0) ON UPDATE CURRENT_TIMESTAMP(0),
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin', '12345678', 'admin@admin.com', 'superadmin', '2021-08-15 22:24:32');
INSERT INTO `users` VALUES (2, 'staff', '12345', 'staff@payroll.com', 'staff', '2021-08-15 00:59:13');
INSERT INTO `users` VALUES (3, 'supervisor', '12345', 'supervisor@payroll.com', 'supervisor', '2021-08-15 00:59:15');

SET FOREIGN_KEY_CHECKS = 1;
