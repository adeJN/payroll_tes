<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('logged_in')) {
            redirect('auth');
        }
        $this->load->model('M_karyawan');
    }

    public function index()
    {
        $this->load->view('temp/header');
        $this->load->view('adm/dashboard');
        $this->load->view('temp/footer');
    }
}
