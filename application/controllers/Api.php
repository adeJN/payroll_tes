<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_users');
    }
    function index()
    {
        $this->db->select('*');
        $this->db->from('presensi');
        echo json_encode($this->db->get()->result());
    }
}
