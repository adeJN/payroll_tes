<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Data</li>
        </ol>
        <!-- Example DataTables Card-->
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-list"></i> Data Karyawan
                <button class="btn btn-success" style="float:right;" data-toggle="modal" data-target="#tambahabsenkaryawan"><i class="fa fa-plus"></i> Tambah Absen Karyawan</button>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Total Lembur</th>
                                <th>NWNP</th>
                                <th>Total Hadir</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Total Lembur</th>
                                <th>NWNP</th>
                                <th>Total Hadir</th>
                                <th>Aksi</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($allKaryawan as $row) {
                            ?>
                                <tr>
                                    <td widtd="5%"><?php echo $no++; ?></td>
                                    <td><?php echo $row->nama; ?></td>
                                    <td><?php echo $row->total_lembur; ?> Jam</td>
                                    <td><?php echo $row->nwnp; ?> Hari</td>
                                    <td><?php

                                        echo $row->total_hadir;
                                        ?> Hari</td>
                                    <td>
                                        <a href="<?php echo base_url(); ?><?= $row->karyawan_id; ?>" class="btn btn-primary" data-toggle="modal" data-target="#modalabsen<?= $row->karyawan_id; ?>"><i class="fa fa-search"></i> Lihat</a>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
    </div>
    <!-- /.container-fluid-->