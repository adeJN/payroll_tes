<div class="modal fade" tabindex="-1" id="tambah" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Karyawan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?php echo form_open('karyawan/tambah'); ?>

                <div class="form-group">
                    <input type="text" name="nama" class="form-control" placeholder="Masukkan Nama">
                </div>
                <div class="form-group">
                    <input type="text" name="tempat_lahir" class="form-control" placeholder="Masukkan Tempat Lahir">
                </div>
                <div class="form-group">
                    <label for="">Tanggal Lahir</label>
                    <input type="date" name="tanggal_lahir" class="form-control" placeholder="Masukkan Tanggal Lahir">
                </div>
                <div class="form-group">
                    <!-- <input type="text" class="form-control" placeholder="Masukkan Jenis Kelamin">   -->
                    <select name="jk" id="" class="form-control">
                        <option value="">Pilih Jenis Kelamin</option>
                        <option value="L">Laki laki</option>
                        <option value="P">Perempuan</option>
                    </select>
                </div>
                <div class="form-group">
                    <input type="number" name="gaji_pokok" class="form-control" placeholder="Masukkan Gaji Pokok">
                </div>
                <div class="form-group">
                    <input type="number" name="tunjangan" class="form-control" placeholder="Masukkan Tunjangan">
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Hapus karyawan Modal-->
<div class="modal fade" id="hapus" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Yakin hapus ?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Pilih yakin jika menghapus data karyawan.</div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Tidak</button>
                <a class="btn btn-danger" href="login.html">Yakin</a>
            </div>
        </div>
    </div>
</div>