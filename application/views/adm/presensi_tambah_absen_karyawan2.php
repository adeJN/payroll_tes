<!-- Modal  -->
<div id="tambahabsenkaryawan" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="<?= site_url('presensi/tambah_absen') ?>" method="post">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Data Absensi </h3>
                    <br>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">

                        <select name="id_karyawan" id="" required class="form-control">
                            <option value="">Pilih Karyawan</option>
                            <?php
                            foreach ($allllllKaryawan as $row) {
                            ?>
                                <option value="<?= $row->karyawan_id ?>"><?= $row->nama ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="">Masukkan tanggal hadir</label>

                        <input type="date" name="tanggal" class="form-control" required>
                    </div>
                    <div class="form-group">
                        <label for="">Masukkan Jumlah lembur</label>
                        <input type="number" min="0" name="lembur" class="form-control" required>
                    </div>
                    <button class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button class="btn btn-primary" type="submit">Submit</button>

            </form>
        </div>
        <div class="modal-footer">
        </div>
    </div>
</div>
</div>