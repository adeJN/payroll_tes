<div class="content-wrapper">
    <div class="container-fluid">
        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="#">Dashboard</a>
            </li>
            <li class="breadcrumb-item active">Data</li>
        </ol>
        <!-- Example DataTables Card-->
        <div class="card mb-3">
            <div class="card-header">
                <i class="fa fa-list"></i> Data Karyawan
                <button class="btn btn-success" style="float:right;" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus"></i> Tambah</button>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Tempat Lahir</th>
                                <th>Tanggal Lahir</th>
                                <th>JK</th>
                                <th>Gaji Pokok</th>
                                <th>Tunjangan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Tempat Lahir</th>
                                <th>Tanggal Lahir</th>
                                <th>JK</th>
                                <th>Gaji Pokok</th>
                                <th>Tunjangan</th>
                                <th>Aksi</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <?php
                            $no = 1;
                            foreach ($allKaryawan as $row) {
                            ?>
                                <tr>
                                    <td widtd="5%"><?php echo $no++; ?></td>
                                    <td><?php echo $row->nama; ?></td>
                                    <td><?php echo $row->tempat_lahir; ?></td>
                                    <td><?php echo $row->tanggal_lahir; ?></td>
                                    <td><?php echo $row->jenis_kelamin; ?></td>
                                    <td><?php echo $row->gaji_pokok; ?></td>
                                    <td><?php echo $row->tunjangan; ?></td>
                                    <td>
                                        <a href="<?php echo base_url(); ?><?= $row->karyawan_id; ?>" class="btn btn-warning" data-toggle="modal" data-target="#modalEdit<?= $row->karyawan_id; ?>"><i class="fa fa-sliders"></i> </a>

                                        <a href="<?= base_url(); ?>karyawan/hapus/<?= $row->karyawan_id; ?>" class="btn btn-danger" onclick="return confirm('Yakin hapus ?')"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
    </div>
    <!-- /.container-fluid-->