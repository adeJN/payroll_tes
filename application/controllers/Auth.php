<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_users');
    }

    public function index()
    {
        if ($this->session->userdata('level') == 'superadmin') {
            redirect('dashboard');
        } elseif ($this->session->userdata('level') == 'staff') {
            redirect('dashboard');
        } elseif ($this->session->userdata('level') == 'supervisor') {
            redirect('dashboard');
        } else {
            $this->load->view('login');
        }
    }
    public function cek_login()
    {
        $data = array(
            'username' => $this->input->post('username', TRUE),
            'password' => $this->input->post('password', TRUE)
        );
        $hasil = $this->M_users->cek_user($data);
        // print_r($hasil);
        if ($hasil->num_rows() == 1) {

            foreach ($hasil->result() as $sess) {
                $sess_data['logged_in'] = true;
                $sess_data['uid'] = $sess->uid;
                $sess_data['username'] = $sess->username;
                $sess_data['level'] = $sess->level;
                $this->session->set_userdata($sess_data);
            }
            redirect('dashboard');
        } else {
            echo "<script>alert('Gagal login, Cek username dan password!');history.go(-1);</script>";
        }
    }
    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('level');
        session_destroy();
        redirect('auth');
    }
}
